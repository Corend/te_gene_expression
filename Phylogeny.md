# Phylogeny of the LTR

We made a ORF prediction + conserved domain to retrieve some RT for each superfamilies.
They are stored in `Reference_RT.fa`.

### Make blastdb with the RT protein bank just created:
```
/src/util/ncbi-blast-2.10.0+/bin/makeblastdb -in Reference_RT.fa -dbtype prot
```

### Blastx my LTR against the RT bank:
```
/src/util/ncbi-blast-2.10.0+/bin/blastx -query TE_Bank.fa -db Reference_RT.fa -out TE_Bank.blastx -evalue 1e-10
```

### Retrieve the RT from the blastx in my LTRs:

Minimum 50 aa ORF.
```
python recup_prot_query_BlastX.py TE_Bank.blastx TE_Bank.blastx.get.fa 50 Olat
```

### Add some reference RT from NCBI:

```
cat RT_ref.fa TE_Bank.blastx.get.fa > RT_ref_my.fa
```

### Align with mafft
```
mafft --thread 2 --threadtb 5 --threadit 0 --reorder --adjustdirection --anysymbol --auto RT_ref_my.fa > RT_only.mafft
```

### Fastree:

```
./FastTree -lg RT_only.mafft > RT_only.tree
```
