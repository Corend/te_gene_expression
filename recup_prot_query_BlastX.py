# -*- coding: utf-8 -*-
# 31/08/17
# Recupere les hits proteiques issus d'un BlastX. NB : ne recupere que le premier match/fragment.

HIT = {}
EV = {}
import sys
# recup_prot_query_BlastX.py hits_blastX_Od.txt[fichier de sortie blastX] hits_blastX_Od.fa[fichier output] 100[longueur minimale des prot recuperees] Od[espece/prefixe]

file = open(sys.argv[1],'r')
res = file.readlines()
file.close()

for r in range(len(res)) :
	lr = res[r].split()
	if len(lr) > 1 :
		if lr[0] == 'Query=':
			name = "_".join(lr)
			#name = lr[1]
			seq = ''
			#print name
			
			s = r+1
			test = 0
			
			while res[s][0:6] != 'Query=' and s < len(res)-1 and test != 2:
				#print s, res[s][0:6]
				if 'Query' in res[s] :
					seq = seq+res[s].split()[2].replace('-','').replace('*','')
				if 'Identities' in res[s] :
					test += 1
				s += 1
			#print seq
			HIT[name] = seq
		
	if 'Sequences' in res[r] :
		#print res[r+2]
		EV[name] = float(res[r+2].strip().split()[2])
		print(EV[name])


file = open(sys.argv[2],'w')
for h in HIT :
	#if len(HIT[h])>=80 and EV[h]<0.00001 :
	if h in EV :
		if len(HIT[h])>=int(sys.argv[3]) and EV[h]<0.00001 :
			if len(sys.argv) == 5 :
				file.write('>'+sys.argv[4]+'_'+h+'\t'+str(EV[h])+'\n'+HIT[h]+'\n')
			else :
				file.write('>'+h+'\t'+str(EV[h])+'\n'+HIT[h]+'\n')
file.close()
