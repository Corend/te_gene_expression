# coding: utf-8

# Packages:
try :
        import argparse
except ImportError as err :
        print ("Unable to find argparse, sorry : {}. Exiting...".format(err))
        exit(1)
try :
        import pandas as pd
except ImportError as err :
        print ("Unable to find pandas, sorry : {}. Exiting...".format(err))
        exit(1)
try :
        import pybedtools as pb
except ImportError as err :
        print ("Unable to find pybedtools, sorry : {}. Exiting...".format(err))
        exit(1)

parser = argparse.ArgumentParser(description=Usage)

parser.add_argument("-g",help="Bedfile of the selected genes, mandatory",required=True)
parser.add_argument("-t",help="Bedfile of the TEs, mandatory",required=True)
parser.add_argument("-i",help="Output file, intersection of the intersected, mandatory",required=True)
args=parser.parse_args()


# Enlarge the bed of the genes
bed = pd.read_csv(args.g,sep="\t",header=None)
bed.columns=['chr','start','end','gene']
bed['startp'] = bed['start'].astype(int)-500000
bed.loc[bed['startp'] < 0, ['startp']] = 0
bed['endp'] = bed['end'].astype(int)+500000
bed[['chr','startp','endp','gene','start','end']].to_csv(args.g+".larger",index=False,sep="\t",encoding="utf-8",header=False)

# Intersect with the TE
genes = pb.BedTool(args.g+".larger")
te = pb.BedTool(args.t)
intersection = genes.intersect(te,wa=True,wb=True).moveto(args.i)

# Then go in R to calc correlation and distance
